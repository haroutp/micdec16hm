using System;

namespace Dec16HM
{
    public class Rectangle : Line
    {
        
        public uint Rows { get; set; }
        public uint Columns { get; set; }

        public void Draw(){
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    System.Console.Write(this.LineCharacter);
                }
                System.Console.WriteLine();
            }
        }
    }
}
