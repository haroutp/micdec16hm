using System;

namespace Dec16HM
{
    public class Arrow : Line
    {

        public char ArrowHead { get => '>'; } 


        public void Draw(){
            this.DrawLine();
            System.Console.WriteLine(ArrowHead);
        }
    }

    
}
