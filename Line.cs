using System;

namespace Dec16HM
{
    public class Line
    {
        private char lineCharacter;
        public uint Size 
        { 
            get; 
            set; 
        }
        public char LineCharacter 
        { 
            get
            {
                return lineCharacter; 
            }
            set{
                lineCharacter = value;
            }
        }

        public void DrawLine(){
            for (int i = 0; i < Size; i++)
            {
                System.Console.Write(LineCharacter);
            }
        }
    }
}
