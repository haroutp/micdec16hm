﻿using System;

namespace Dec16HM
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle r = new Rectangle {LineCharacter = '*', Rows = 15, Columns = 33};

            r.Draw();

            Square s = new Square {LineCharacter = 'W', Size = 3};

            s.Draw();



            // Arrow a = new Arrow { Size = 23, LineCharacter = '-' };
            // a.Draw();
            // DoubleArrow da = new DoubleArrow {Size = 17, LineCharacter = '='};

            // da.Draw();



        }
    }
}
